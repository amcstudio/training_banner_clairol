'use strict';

/*!
GULP DEPENDENCIES 
npm install -g gulp gulp-concat  gulp-uglify gulp-replace-path gulp-inline-source gulp-autoprefixer gulp-rename gulp-sass gulp-useref gulp-notify gulp-plumber gulp-zip gulp-bump gulp-confirm  gulp-insert-lines gulp-dom del fs path  run-sequence browser-sync
sudo npm link gulp gulp-concat  gulp-uglify gulp-replace-path gulp-inline-source gulp-autoprefixer gulp-rename gulp-sass gulp-useref gulp-notify gulp-plumber gulp-zip gulp-bump gulp-confirm  gulp-insert-lines gulp-dom del fs path  run-sequence browser-sync 
*/

const gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    useref = require('gulp-useref'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    zip = require('gulp-zip'),
    bump = require('gulp-bump'),
    confirm = require('gulp-confirm'),
    dom = require('gulp-dom'),
    inlinesource = require('gulp-inline-source'),
    replacePath = require('gulp-replace-path'),

    del = require('del'),
    fs = require('fs'),
    path = require('path'),
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync');


var json,
    datajson,
    filePath = path.basename(__dirname),
    developer,
    adServerSrc,
    adServerFTSrc,
    thisVersion,
    adServer,
    bannerWidth=300,
    bannerHeight=250,
    footTag,
    manifestFile,
    manifestTag,
    politeIndexTag,
    politeDir = './ft_polite',
    animationSrc;

function doHeadTags(answer) {
    datajson = JSON.parse(fs.readFileSync('./data.json'))

    switch (answer) {
        case "1":
            // Double Click Studio
            adServerSrc = datajson.doubleClick.cdn;
            animationSrc = datajson.animationTag.jsTag;
            footTag = datajson.doubleClick.standardDC;
            break;
        case "2":

            // Double Click Studio - Polite Load
            adServerSrc = datajson.doubleClick.cdn;
            animationSrc = datajson.animationTag.jsTag;
            footTag = datajson.doubleClick.polite;
            break;
        case "3":
            // DCM 
            animationSrc = datajson.animationTag.jsTag;
            footTag = datajson.standard.adserverJs;
            break;
        case "4":
            adServerSrc = datajson.flashTalking.cdn;
            animationSrc = datajson.animationTag.jsTag;
            footTag = datajson.standard.adserverJs;
            gulp.start('ft-manifest-standered');
            break;
        case "5":
            // FLASH TALKING RICH
            adServerFTSrc = datajson.flashTalking.cdn;
            animationSrc = datajson.animationTag.jsTag;
            footTag = datajson.standard.adserverJs;
            gulp.start('mk-ft-polite');

            break;
        case "6":
            // polite Load js DoubleClick
            adServerSrc = datajson.doubleClick.cdn;
            footTag = datajson.doubleClick.jsPolite;
            gulp.start('mk-dc-politeLoad');
            break;
        case "7":
            // polite Load js DoubleClick
            adServerSrc = datajson.sizmek.cdn;
            footTag = datajson.sizmek.jsPolite;
            gulp.start('mk-sizmek-politeLoad');
            break;
        default:
            footTag = "window.onload = init();";
            break;

    }
    writeAdserver(footTag, answer);
    gulp.start('insert-dom-bundle');


}

/*--------------------------------------------------
 CONCATINATE THE JAVASCRIPT INTO ONE MINIFIED FILE
--------------------------------------------------*/

gulp.task("concatScripts", function() {
    var alljsFiles = ['src/js/vendor/*',
            'src/js/olsTween*.js', 'src/js/adservers.js',
            '!src/js/politeLoadAnimation.js'
        ],
        animationjsFile = 'src/js/animation.js';

    if (fs.existsSync(animationjsFile)) {
        alljsFiles.push(animationjsFile);
    }
    return gulp.src(alljsFiles)
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('deploy/js'))

    .pipe(notify({
        message: 'Scripts task complete'
    }));
});

gulp.task("movePoliteScripts", function() {
    return gulp.src([
            './src/js/politeLoadAnimation.js'
        ])
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest('deploy/js'))

    .pipe(notify({
        message: 'Scripts task complete'
    }));
});

/*--------------------------------------------------
COMPILE SASS AND MINIFY INTO ONE FILE IN DEPLOY
--------------------------------------------------*/

gulp.task('compileSass', function() {
    return gulp.src(['./src/scss/main.scss'])
        .pipe(plumber())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('src/css'))
        .pipe(notify({
            message: 'Sass task complete'
        }))
});


/*--------------------------------------------------
MOVE ALL ASSETS INTO THE DEPLOY FOLDER
//

//


--------------------------------------------------*/
gulp.task('moveFiles', function() {
    return gulp.src(["src/*", 'src/img/*', 'src/fonts/*', 'src/css/fonts/*', 'src/css/*', '!src/scss/', '!src/index.html'], {
            base: './src'
        })
        .pipe(plumber())
        .pipe(gulp.dest('deploy'));
})

/*--------------------------------------------------
MOVE HTML IN TO THE THE DEPLOY FOLDER & UPDATE LINKS
--------------------------------------------------*/

gulp.task('moveHTML', function() {
    return gulp.src('src/index.html')
        .pipe(useref())
        .pipe(gulp.dest('deploy'));
})


/*--------------------------------------------------
CREATE ARCHIVES
--------------------------------------------------*/

gulp.task('createSrcArchive', function() {
    json = JSON.parse(fs.readFileSync('./package.json'))
    return gulp.src('src/**/*')
        .pipe(zip(filePath + "-versionArchive-v" + json.version + '.zip'))
        .pipe(gulp.dest('./_versionArchive/'));

});

/*--------------------------------------------------
CLEAN OUT THE DEPLOY FOLDER 
--------------------------------------------------*/
gulp.task('clean', function() {
    del(['_publishZip/', './deploy/**/*', '.DS_Store', '.DS_Store?', '._*', '.Spotlight-V100', '.Trashes', 'ehthumbs.db', 'Thumbs.db']);
});

/*--------------------------------------------------
WATCH FUNCTION
--------------------------------------------------*/

gulp.task('watchFiles', function() {
    gulp.watch('src/scss/**/*.scss', ['compileSass']);
    gulp.watch('src/js/**/*.js', ['concatScripts', 'movePoliteScripts']);
    gulp.watch(['src/**/*'], ['moveFiles']);
    //   gulp.watch(['src/img/*.png'], ['optimise-images']);




    gulp.watch('src/index.html', ['moveHTML']);
    gulp.watch('deploy/**/**/*', ['inlinesource', 'bs-reload']);

});

gulp.task('dev', function() {
    return gulp.src('src/**/*')
        .pipe(confirm({
            question: 'Developer name? (press enter if you are re-opening the same file)',
            proceed: function(answer) {
                developer = answer;
                return true;
            }
        }))
});


gulp.task('server', function() {
    json = JSON.parse(fs.readFileSync('./package.json'));
    var version = json.version.toString;

    if (json.version === '0.0.0') {
        gulp.src('src/**/*')
            .pipe(confirm({
                question: 'Ad Server? Press | Doubleclick: 1 | Doubleclick polite: 2 |  standard: 3 | FT standard: 4 | FT rich: 5 | DCS polite: 6 | Sizmek: 7',
                proceed: function(answer) {

                    doHeadTags(answer);

                    return true;
                }
            }))

    }
    // else {
    //     fs.readFile(serverFile, 'utf8', function(err, data) {
    //         doHeadTags(data);
    //     });
    // }
    return true;

});


gulp.task('version', function() {

    json = JSON.parse(fs.readFileSync('./package.json'))

    return gulp.src('src/**/*')
        .pipe(confirm({
            question: 'Version Number?, current version is ' + json.version + ' (press enter if you are re-opening the same file)',
            proceed: function(answer) {
                if (answer !== "" && answer !== json.version && json.version !== "0.0.0") {
                    gulp.start('createSrcArchive')
                }
                thisVersion = answer || json.version;

                return true;
            }
        }))
});





// /*--------------------------------------------------
// create Ft manifest g+ulp
// --------------------------------------------------*/
gulp.task('ft-manifest-standered', function() {

    manifestTag = datajson.flashTalking.manifestStandard;

    fs.writeFile('src/manifest.js', manifestTag);
});

// /*--------------------------------------------------
// create ft polite files
// --------------------------------------------------*/
gulp.task('mk-ft-polite', function() {

    if (!fs.existsSync(politeDir)) {
        fs.mkdirSync(politeDir);
    }

    manifestTag = datajson.flashTalking.manifestRich;

    manifestTag = replaceString(manifestTag, 'filePath', filePath);

    fs.writeFile('./ft_polite/manifest.js', manifestTag);
    gulp.start('mk-ft-politeIndex')
});
gulp.task('mk-ft-politeIndex', function() {
    if (!fs.existsSync(politeDir)) {
        fs.mkdirSync(politeDir);
    }

    politeIndexTag = datajson.flashTalking.richIndexStyle;
    return gulp.src('./adservers/ft_rich/index.html')
        .pipe(dom(function() {

            this.getElementsByTagName('style')[0].innerHTML = politeIndexTag;
            if (adServerFTSrc != undefined) {
                this.getElementById('adServerTag').setAttribute('src', adServerFTSrc);
            }
            return this;
        }))

    .pipe(gulp.dest('./ft_polite'));

});

// DoubleClick polite load load images through js




gulp.task('mk-dc-politeLoad', ['mk-dc-politejs'], function() {

    var fileDir = 'src/js/animation.js',
        fileExists = fs.existsSync(fileDir);
    if (animationSrc === undefined && json.version === '0.0.0') {
        if (fileExists) {
            return del(fileDir);
        }
    }


});
gulp.task('mk-dc-politejs', function() {

    if (!fs.existsSync('./src/js/politeLoadAnimation.js')) {

        return gulp.src('./adservers/dc_polite/politeLoadAnimation.js')

        .pipe(gulp.dest('./src/js'));
    }


});

gulp.task('mk-sizmek-politeLoad', function() {

    if (!fs.existsSync('./src/js/EBLoader.js')) {

        return gulp.src('./adservers/sizmek/EBLoader.js')

        .pipe(gulp.dest('./src/js'));
    }


});


/*--------------------------------------------------
DEFAULT
--------------------------------------------------*/
gulp.task('default', ['server', 'dev', 'version'], function() {
    gulp.start('dotasks');
    if (developer !== undefined) {
        gulp.start('addDev');
    }
});

gulp.task('dotasks', function(err) {
    runSequence(['bumpVersion'], ['moveFiles', 'moveHTML', 'watchFiles'], ['concatScripts', 'movePoliteScripts', 'compileSass'], 'browser-sync');
})
gulp.task('open', function(err) {
        runSequence(['moveFiles', 'moveHTML', 'watchFiles'], ['concatScripts', 'movePoliteScripts', 'compileSass'], 'browser-sync');
    })
    /*--------------------------------------------------
    pubish FT Zip
    --------------------------------------------------*/
gulp.task('publishft', ['cleanSys'], function() {
    json = JSON.parse(fs.readFileSync('./package.json'))

    publishDeploy(json);
    publishBase(json)
});

function publishDeploy(json) {
    return gulp.src(['./deploy/**/*'], {
        base: './deploy'
    })

    .pipe(zip(filePath.replace('.', '_') + "_rich.zip"))
        .pipe(gulp.dest('./_publishZip'));
}

function publishBase(json) {
    return gulp.src(['./ft_polite/**/*'], {
        base: './ft_polite'
    })

    .pipe(zip(filePath.replace('.', '_') + "_base.zip"))
        .pipe(gulp.dest('./_publishZip'));
}

/*--------------------------------------------------
pubishZip
--------------------------------------------------*/
gulp.task('publish', ['cleanSys'], function() {
    json = JSON.parse(fs.readFileSync('./package.json'))
      var currentVersion = json.version;
    currentVersion = currentVersion.substr(0, currentVersion.lastIndexOf('.'));
    return gulp.src('./deploy/**/*', {
        base: './deploy'
    })

    .pipe(zip(filePath + "_V" + currentVersion + "_publish.zip"))

    .pipe(gulp.dest('./_publishZip'));

});

gulp.task('cleanSys', function() {
    return del(['./_publishZip/', './deploy/**/.*', './deploy/**/_*', './deploy/**/*.db'])
});


function writeAdserver(serverTag, ans) {

    fs.writeFile('./src/js/adservers.js', serverTag, function(err) {});
    fs.writeFile('./adservers/.server.txt', ans, function(err) {});
}


/*--------------------------------------------------
INLINE CSS AND JS
--------------------------------------------------*/
gulp.task('publishInline', function() {
    runSequence(['cleanSys', 'moveInline', 'inlinesource'], 'replacePath', 'packageInlineZip', 'removeInlineFolder');

});

gulp.task('removeInlineFolder', function() {
    return del('./inline');
})
gulp.task('packageInlineZip', function() {
    json = JSON.parse(fs.readFileSync('./package.json'))

    return gulp.src(['./inline/index.html',
        './inline/img/*',
        './inline/fonts/*'
    ], {
        base:'./inline'
    })

    .pipe(zip(filePath + "_" + json.version + "_publish.zip"))
        .pipe(gulp.dest('./_publishZip'));
})

gulp.task('replacePath', function() {
    return gulp.src(['./inline/index.html'])
        .pipe(replacePath('../img/', 'img/'))
        .pipe(replacePath('../fonts/', 'fonts/'))
        .pipe(gulp.dest('./inline'));
});


gulp.task('moveInline', function() {
    return gulp.src(["deploy/img/**/*", 'deploy/fonts/**/*'], {
            base: './deploy'
        })
        .pipe(plumber())
        .pipe(gulp.dest('./inline'));
})


gulp.task('inlinesource', function() {
    return gulp.src('./deploy/index.html')
        .pipe(inlinesource())
        .pipe(gulp.dest('./inline'));
});


/*--------------------------------------------------
Minor version increment
--------------------------------------------------*/

gulp.task('bumpVersion', function() {
    gulp.src('./package.json')
        .pipe(bump({
            version: thisVersion
        }))
        .pipe(gulp.dest('./'));
});



gulp.task('addDev', function(cb) {
    fs.appendFile('.contributors', '\n' + developer + ' | v-' + thisVersion + ' | ' + datetime, function(err) {});
});


// /*--------------------------------------------------
// Insert dom
// --------------------------------------------------*/

gulp.task('insert-dom-bundle', function() {

    return gulp.src('./src/index.html')
        .pipe(dom(function() {

            if (adServerSrc != undefined) {
                this.getElementById('adServerTag').setAttribute('src', adServerSrc);
            }

            if (animationSrc != undefined) {

                this.getElementById('animationTag').setAttribute('src', animationSrc);
            }

            if (filePath) {
                this.getElementsByTagName('title')[0].innerHTML = filePath;
            }
            return this;
        }))
        //

    .pipe(gulp.dest('./src/'));

});



// /*--------------------------------------------------
// Browser Sync
// --------------------------------------------------*/
gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: "./src"
        }
    });
});
gulp.task('bs-reload', function() {
    browserSync.reload();
});


// /*--------------------------------------------------
// gulp Resize
// --------------------------------------------------*/
gulp.task('resize', function() {
     thisVersion= '1.0.0';
     runSequence(['bumpVersion'],['rmVersionArchive','get-banner-width','get-banner-height','dev'],['addDev','resizeInSass','insert-dom-resize','open']);
});

gulp.task('rmVersionArchive', function() {
   
    return del([ './_versionArchive'])
});

gulp.task('get-banner-width', function() {
    json = JSON.parse(fs.readFileSync('./package.json'))
    datajson = JSON.parse(fs.readFileSync('./data.json'))

    return gulp.src('./src/')
        .pipe(confirm({
            question: 'Banner width? ',
            proceed: function(answer) {
                if (answer) {
                    bannerWidth = answer;
                } else {
                    bannerWidth = 300;
                }
                return true;
            }
        }))

});

//
gulp.task('get-banner-height', function() {
    json = JSON.parse(fs.readFileSync('./package.json'))
    datajson = JSON.parse(fs.readFileSync('./data.json'))
    return gulp.src('./src/')
        .pipe(confirm({
            question: 'Banner height? ',
            proceed: function(answer) {
                if (answer) {
                    bannerHeight = answer;
                } else {
                    bannerHeight = 250;
                }

                return true;
            }
        }))

});

gulp.task('resizeInSass', function() {
 fs.writeFile('./src/scss/_settings.scss', '', function(){console.log('removedContentSetting')})
 fs.appendFile('./src/scss/_settings.scss', '\n' +'$width:'+bannerWidth+'px;'+'\n'+'$height:'+bannerHeight+'px;', function(err) {});

});
gulp.task('insert-dom-resize', function() {

    return gulp.src('./src/index.html')
        .pipe(dom(function() {

    
            if (filePath) {
                this.getElementsByTagName('title')[0].innerHTML = filePath;
            }
            if (bannerWidth != undefined && bannerHeight != undefined) {
                this.getElementsByName('ad.size')[0].setAttribute('content', "width=" + bannerWidth + "\,height=" + bannerHeight);
            }
            return this;
        }))
        //

    .pipe(gulp.dest('./src/'));

});


// /*--------------------------------------------------
// DATE TIME FUNCTION FOR ARCHIVE NAMING
// --------------------------------------------------*/

var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"]
var hrs = ["12am", "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12pm", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm", "12pm"]
var currentdate = new Date();
var datetime = currentdate.getDate() + " " + months[currentdate.getMonth()] + " " + currentdate.getFullYear() + " at " + hrs[currentdate.getHours()];


// /*--------------------------------------------------
// replace string with another
// --------------------------------------------------*/
function replaceString(target, search, replacement) {
    var str = target;
    return str.replace(new RegExp(search, 'g'), replacement);
}