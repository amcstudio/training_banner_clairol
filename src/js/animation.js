'use strict';
~ function() {
  var $ = TweenMax,
  path = document.getElementsByClassName('path'),
  color = document.getElementsByClassName('st0'),

  ad = document.getElementById('mainContent'),
  easeIn = Power1.easeIn,
  easeOut = Power1.easeOut;
  
  window.init = function() {
    var t1 = new TimelineMax();
    t1.set('svg', {opacity:1, ease:easeOut},0)

    for (var i = 0; i < path.length; i++) {

      path[i].style.strokeDasharray = path[i].getTotalLength()+15;
      path[i].style.strokeDashoffset = path[i].getTotalLength()+30;
      // console.log(path[i]);
    }
    play();
  }

  function play() {
    var tl = new TimelineMax();

     tl.to('#logo', 0.75 ,{y:0, ease:easeOut},0)
     tl.to('#copyOne', 0.75 ,{x:0, rotation:0.01, ease:easeOut},0.75)
      setTimeout(svgCopy, 1250, color, 0.5)
   	 
}

function svgCopy(elem, time){
   var tw = new TimelineMax();
   for (var i = 0; i < elem.length; i++) {
    tw.to(elem[i], time, {strokeDashoffset: 0, ease: Power1.easeOut})
  }

}

  

}();
